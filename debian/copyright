Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tilp
Upstream-Contact: Lionel Debroux <lionel_debroux@yahoo.fr>
Source: https://sourceforge.net/projects/tilp
Comment: This package was created by Romain LIEVIN <roms@tilp.info> in
 2002.  It was taken over by Krzysztof Burghardt <krzysztof@burghardt.pl>
 around 2007.  Since 2012 Albert Huang <alberth.debian@gmail.com> worked
 on the package.  It is now team-maintained in Debian-Science.
 .
 Upstream Authors: Romain Liévin <roms@tilp.info>
                   Julien Blache <jb@jblache.org>
                   Thomas Corvazier
                   Jesse Palmer <jp@jp3d.net>
                   Kevin Kofler <Kevin@tigcc.ticalc.org>
                   Lauris Kaplinski <lauris@kaplinski.com>
                   Sylvain Garsault
                   Tyler Cassidy <tyler@tylerc.org>
                   Tijl Coosemans <tijl@ulyssis.org>
                   Benjamin Moody <benjamin@ecg.mit.edu>

Files: *
Copyright: 1999-2008 Romain Liévin
           1999-2005 Julien Blache
           2000-2001 Thomas Corvazier
           2002 Jesse Palmer
           2003-2007 Kevin Kofler
           2003-2006 Lauris Kaplinski
           2003 Sylvain Garsault
           2006 Tyler Cassidy
License: GPL-2+

Files: debian/*
Copyright: 2002-2009 Romain Liévin <roms@tilp.info>,
           2009 Krzysztof Burghardt <krzysztof@burghardt.pl>,
           2012 Albert Huang <alberth.debian@gmail.com>,
           2013, 2018, 2022 Andreas B. Mundt <andi@debian.org>.
License: GPL-2+

Files: desktop/tilp.appdata.xml
Copyright: 2020 Lionel Debroux
	   2020 Ben Rosser <rosser.bjr@gmail.com>
License: CC0-1.0
Comment: The metadata_license field in the header of this file suggested it
 is CC0-1.0, and John Scott <jscott@posteo.net> verified with the maintainers
 by email that this is correct.
 Sent January 3, 2020 by Ben Rosser:
 Hi John,
 .
 Yes, I wrote the file (as I was encouraged to do so by Fedora's
 packaging guidelines).
 .
 I believe we shipped the metadata file in Fedora before it was added
 upstream; I think I just defaulted to CC0 as that's the license used
 in the Fedora example [1], and I think the appstream validation tools
 want metadata_license to be a permissive license. (That seems to be
 what the documentation [2] claims).
 .
 Unless Lionel says anything to the contrary, I would assume it's still CC0.
 .
 Ben Rosser
 [1] https://docs.fedoraproject.org/en-US/packaging-guidelines/AppData/
 [2] https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-metadata_license
 .
 and Lionel said he had no issues with Ben's choice.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: CC0-1.0
 On Debian systems, the full text of the CC0-1.0 license can be found in
 the file `/usr/share/common-licenses/CC0-1.0'.
